import "./CatPage.css";
import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";

// Afficher les chats provenant de thecatapi.com et la pagination  

const CatPage = () => {

    const [page, setPage] = useState(1);
    const [dataCat, setDataCat] = useState([]);
    const fetchCat = async (currentPage) => {
        const res = await fetch(`https://api.thecatapi.com/v1/images/search?page=${currentPage}&order=Desc&limit=8`,{
          method: "GET",
          headers: {
            "x-api-key": "f4d88ccc-ace3-4f8b-8611-1456b2c9ae54"
        },}
        )

    const data = await res.json();
        return data;
      };

    const changePage = async (data) => {
    let pageCourante = data.selected + 1;
    const setPageCourante = await fetchCat(pageCourante);
        setDataCat(setPageCourante);
      };

      useEffect(() => {
    const updateCatValue = async () => {
            setDataCat(await fetchCat(page));
        }
        updateCatValue();
    }, [page]);

    // Ce qui s'affiche sur le site 

    return (
        <>
        <h1 class="animate__heartBeat">Bienvenue sur mon application React</h1>
        <h4>Et voici, les plus beaux félins du monde !</h4>

        <div className="catCard">
        {dataCat.map(cat => (
            <img key={cat.id} src={cat.url}/>
        ))}
        </div>

        <ReactPaginate
            previousLabel={'Precedant'}
            nextLabel={'Suivant'}
            containerClassName={'my_button'}
            onPageChange={changePage}
            pageCount={15}
            // les class pour le css
            pageClassName={'page'}
            activeClassName={'active'}
            disabledClassName={'disabled'}
        />
        </>
    )
}

export default CatPage;
